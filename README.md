# pyoctoprintapifacade
Facade for communication with printer through the Octoprint API

Install package
--------------
* Linux
```
python3 -m pip install git+https://gitlab.com/belittlemaker/py-octoprint-api-facade.git@master
```
* Windows
```
py -m pip install git+https://gitlab.com/belittlemaker/py-octoprint-api-facade.git@master
```
Usage examples
--------------
```python
from pyoctoprintapifacade import OctoPrintApi

# own connection data to Octoprint API
# https://docs.octoprint.org/en/master/api/general.html
api_key = ''
printer_domain = '127.0.0.1/api'

printer_api = OctoPrintApi(api_key, printer_domain)

# connection, port: optional parameter
printer_api.get_connection_facade().connect(port='/dev/ttyUSB1')
# disconnect
printer_api.get_connection_facade().disconnect()

# go to home-axis (0,0,0)
printer_api.get_printer_facade().go_home()

# cancel job
printer_api.get_job_facade().cancel()
# pause, resume, toggle job
printer_api.get_job_facade().pause()
printer_api.get_job_facade().resume()
printer_api.get_job_facade().toggle()

# heating bed
printer_api.get_printer_facade().bed_target(50)
# heating tool
printer_api.get_printer_facade().tool_target({'tool0': 200})
# bed and tool cooling
printer_api.get_printer_facade().bed_target(0)
printer_api.get_printer_facade().tool_target({'tool0': 0})

# move axis
printer_api.get_printer_facade().jog(x=10, y=20, speed=1200)
printer_api.get_printer_facade().jog(x=-10, speed=1200)
printer_api.get_printer_facade().jog(z=50, speed=1200)
printer_api.get_printer_facade().jog(x=10, y=20, z=50)

# enable logging traces
printer_api.enable_logging_traces()

```
