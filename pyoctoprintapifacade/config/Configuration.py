
class Configuration:

    PROTOCOL_HTTPS = 'https'
    PROTOCOL_HTTP = 'http'

    def __init__(self, api_key: str, domain: str):
        self.__api_key: str = api_key
        self.__domain: str = domain
        self.__protocol: str = self.PROTOCOL_HTTP

    def enable_https_protocol(self):
        self.__protocol = self.PROTOCOL_HTTPS

    def get_api_key(self) -> str:
        return self.__api_key

    def get_domain(self) -> str:
        return self.__domain

    def get_protocol(self) -> str:
        return self.__protocol
