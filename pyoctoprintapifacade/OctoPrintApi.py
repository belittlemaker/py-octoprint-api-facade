from .config import Configuration
from .facade import ConnectionFacade
from .facade import JobFacade
from .facade import PrinterFacade
from .sender import Sender


class OctoPrintApi:
    """ https://docs.octoprint.org/en/master/api/ """

    __config = None
    __connection_facade = None
    __job_facade = None
    __printer_facade = None
    __sender = None

    def __init__(self, api_key: str, domain: str):
        self.__set_config(api_key, domain)

    def enable_https(self):
        self.__config.enable_https_protocol()

    def enable_logging_traces(self):
        self.__get_sender().enable_sender_trace()

    def get_connection_facade(self) -> ConnectionFacade:
        if not self.__connection_facade:
            self.__connection_facade = ConnectionFacade(self.__config, self.__get_sender())
        return self.__connection_facade

    def get_job_facade(self) -> JobFacade:
        if not self.__job_facade:
            self.__job_facade = JobFacade(self.__config, self.__get_sender())
        return self.__job_facade

    def get_printer_facade(self) -> PrinterFacade:
        if not self.__printer_facade:
            self.__printer_facade = PrinterFacade(self.__config, self.__get_sender())
        return self.__printer_facade

    def __get_sender(self) -> Sender:
        if not self.__sender:
            self.__sender = Sender()
        return self.__sender

    def __set_config(self, api_key: str, domain: str):
        self.__config = Configuration(api_key, domain)
