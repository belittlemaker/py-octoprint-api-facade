from ..constant import ConstGeneral
from ..exception import OctoResponseException
from ..exception import OctoUnexpectedException
from ..request import AbstractRequest
import logging
from requests import Response


class Sender:

    def __init__(self):
        self.__traces: bool = False

    def send(self, request: AbstractRequest) -> Response:
        try:
            response = request.get_response()
        except Exception as exception:
            raise OctoResponseException(str(exception))

        response.encoding = ConstGeneral.ENCODING

        self.__logging_traces(request, response)
        self.__check_response(response)

        return response

    def enable_sender_trace(self):
        self.__traces: bool = True

    @staticmethod
    def __check_response(response: Response) -> None:
        allowed_status_code = [ConstGeneral.STATUS_CODE_OK,
                               ConstGeneral.STATUS_CODE_OK_NO_CONTENT]
        status_code = int(response.status_code)

        if status_code not in allowed_status_code:
            raise OctoUnexpectedException(response.text)

    def __logging_traces(self, request: AbstractRequest, response: Response) -> None:
        if self.__traces:
            logging.debug(request.get_url())
            logging.debug(str(request.get_params()))
            logging.debug(response.text)
