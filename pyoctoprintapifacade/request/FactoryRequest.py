from .AbstractRequest import AbstractRequest
from .RequestConnectionGet import RequestConnectionGet
from .RequestConnectionPost import RequestConnectionPost
from .RequestPrinter import RequestPrinter
from .RequestPrinterBed import RequestPrinterBed
from .RequestPrinterCommand import RequestPrinterCommand
from .RequestPrinterPrintHead import RequestPrinterPrintHead
from .RequestPrinterTool import RequestPrinterTool
from .RequestJob import RequestJob
from .RequestJobPost import RequestJobPost
from ..config import Configuration


class FactoryRequest:

    def __init__(self, config: Configuration):
        self.__config: Configuration = config

    def get_request_connection_get(self) -> AbstractRequest:
        return RequestConnectionGet(self.__config)

    def get_request_connection_post(self) -> AbstractRequest:
        return RequestConnectionPost(self.__config)

    def get_request_job(self) -> AbstractRequest:
        return RequestJob(self.__config)

    def get_request_job_post(self) -> AbstractRequest:
        return RequestJobPost(self.__config)

    def get_request_printer(self) -> AbstractRequest:
        return RequestPrinter(self.__config)

    def get_request_printer_command(self) -> AbstractRequest:
        return RequestPrinterCommand(self.__config)

    def get_request_printer_bed(self) -> RequestPrinterBed:
        return RequestPrinterBed(self.__config)

    def get_request_printer_printhead(self) -> RequestPrinterPrintHead:
        return RequestPrinterPrintHead(self.__config)

    def get_request_printer_tool(self) -> RequestPrinterTool:
        return RequestPrinterTool(self.__config)
