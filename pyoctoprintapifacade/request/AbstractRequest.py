from abc import ABCMeta, abstractmethod
from requests import Response
from ..config import Configuration
from ..constant import ConstGeneral


class AbstractRequest(metaclass=ABCMeta):

    def __init__(self, config: Configuration):
        self.__config: Configuration = config
        self._params: dict = {}

    def get_url(self) -> str:
        return self.__config.get_protocol() + '://' + self.__config.get_domain() + '/' + self._get_slug()

    def get_params(self) -> dict:
        return self._params

    def set_params(self, params: dict) -> None:
        for key_param in self._get_allowed_keys_params():
            if key_param in params:
                self._params[key_param] = params[key_param]

    @staticmethod
    def _get_allowed_keys_params() -> list:
        return []

    def get_headers(self) -> dict:
        return {ConstGeneral.HEADER_KEY_CONTENT_TYPE: ConstGeneral.HEADER_CONTENT_TYPE_VALUE,
                ConstGeneral.HEADER_KEY_X_API_KEY: self.__config.get_api_key()}

    @abstractmethod
    def get_method(self) -> str:
        pass

    @abstractmethod
    def get_response(self) -> Response:
        pass

    @abstractmethod
    def _get_slug(self) -> str:
        pass
