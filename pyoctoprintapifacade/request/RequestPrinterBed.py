from ..constant import ConstGeneral
from .AbstractRequestPost import AbstractRequestPost


class RequestPrinterBed(AbstractRequestPost):

    def _get_slug(self) -> str:
        return 'printer/bed'

    @staticmethod
    def _get_allowed_keys_params() -> list:
        return [ConstGeneral.POST_KEY_COMMAND,
                ConstGeneral.PRINTER_KEY_TARGET,
                ConstGeneral.PRINTER_KEY_OFFSET]
