from ..constant import ConstGeneral
from .AbstractRequestPost import AbstractRequestPost


class RequestPrinterCommand(AbstractRequestPost):

    def _get_slug(self) -> str:
        return 'printer/command'

    @staticmethod
    def _get_allowed_keys_params() -> list:
        return [ConstGeneral.POST_KEY_COMMANDS]
