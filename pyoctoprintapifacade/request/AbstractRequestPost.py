from abc import ABCMeta
from .AbstractRequest import AbstractRequest
from ..constant import ConstGeneral
import requests


class AbstractRequestPost(AbstractRequest, metaclass=ABCMeta):

    def get_method(self) -> str:
        return ConstGeneral.HTTP_METHOD_POST

    def get_response(self) -> requests.Response:
        return requests.post(self.get_url(),
                             headers=self.get_headers(),
                             json=self.get_params())
