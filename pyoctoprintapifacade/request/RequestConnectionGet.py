from .AbstractRequestGet import AbstractRequestGet


class RequestConnectionGet(AbstractRequestGet):

    def _get_slug(self) -> str:
        return 'connection'
