from ..constant import ConstGeneral
from .AbstractRequestPost import AbstractRequestPost


class RequestJobPost(AbstractRequestPost):

    def _get_slug(self) -> str:
        return 'job'

    @staticmethod
    def _get_allowed_keys_params() -> list:
        return [ConstGeneral.POST_KEY_COMMAND,
                ConstGeneral.POST_KEY_ACTION]
