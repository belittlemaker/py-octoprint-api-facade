from ..constant import ConstGeneral
from .AbstractRequestPost import AbstractRequestPost


class RequestPrinterTool(AbstractRequestPost):

    def _get_slug(self) -> str:
        return 'printer/tool'

    @staticmethod
    def _get_allowed_keys_params() -> list:
        return [ConstGeneral.POST_KEY_COMMAND,
                ConstGeneral.PRINTER_KEY_TARGETS,
                ConstGeneral.PRINTER_KEY_OFFSETS]
