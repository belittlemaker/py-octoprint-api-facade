from ..constant import ConstGeneral
from .AbstractRequestPost import AbstractRequestPost


class RequestConnectionPost(AbstractRequestPost):

    def _get_slug(self) -> str:
        return 'connection'

    @staticmethod
    def _get_allowed_keys_params() -> list:
        return [ConstGeneral.CONNECTION_POST_KEY_AUTOCONNECT,
                ConstGeneral.CONNECTION_POST_KEY_BAUDRATE,
                ConstGeneral.CONNECTION_POST_KEY_COMMAND,
                ConstGeneral.CONNECTION_POST_KEY_PRINTER_PROFILE,
                ConstGeneral.CONNECTION_POST_KEY_PORT,
                ConstGeneral.CONNECTION_POST_KEY_SAVE]
