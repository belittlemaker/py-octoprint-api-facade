from .AbstractRequestGet import AbstractRequestGet


class RequestJob(AbstractRequestGet):

    def _get_slug(self) -> str:
        return 'job'
