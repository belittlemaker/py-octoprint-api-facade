from abc import ABCMeta
from .AbstractRequest import AbstractRequest
from ..constant import ConstGeneral
import requests


class AbstractRequestGet(AbstractRequest, metaclass=ABCMeta):

    def get_method(self) -> str:
        return ConstGeneral.HTTP_METHOD_GET

    def get_response(self) -> requests.Response:
        return requests.get(self.get_url(),
                            headers=self.get_headers(),
                            params=self.get_params())
