from .AbstractRequestGet import AbstractRequestGet


class RequestPrinter(AbstractRequestGet):

    def _get_slug(self) -> str:
        return 'printer'
