from ..constant import ConstGeneral
from .AbstractRequestPost import AbstractRequestPost


class RequestPrinterPrintHead(AbstractRequestPost):

    def _get_slug(self) -> str:
        return 'printer/printhead'

    @staticmethod
    def _get_allowed_keys_params() -> list:
        return [ConstGeneral.POST_KEY_COMMAND,
                ConstGeneral.PRINTHEAD_POST_KEY_ABSOLUTE,
                ConstGeneral.PRINTHEAD_POST_KEY_AXES,
                ConstGeneral.PRINTHEAD_POST_KEY_SPEED,
                ConstGeneral.PRINTHEAD_POST_KEY_X,
                ConstGeneral.PRINTHEAD_POST_KEY_Y,
                ConstGeneral.PRINTHEAD_POST_KEY_Z]

    def enable_absolute(self):
        self._params[ConstGeneral.PRINTHEAD_POST_KEY_ABSOLUTE] = True

    def enable_home_axes(self):
        if ConstGeneral.PRINTHEAD_POST_KEY_AXES not in self._params:
            self._params[ConstGeneral.PRINTHEAD_POST_KEY_AXES] = []

    def enable_home_axes_x(self):
        self.enable_home_axes()
        if ConstGeneral.PRINTHEAD_AXES_VALUE_X not in self._params[ConstGeneral.PRINTHEAD_POST_KEY_AXES]:
            self._params[ConstGeneral.PRINTHEAD_POST_KEY_AXES].append(ConstGeneral.PRINTHEAD_AXES_VALUE_X)

    def enable_home_axes_y(self):
        self.enable_home_axes()
        if ConstGeneral.PRINTHEAD_AXES_VALUE_Y not in self._params[ConstGeneral.PRINTHEAD_POST_KEY_AXES]:
            self._params[ConstGeneral.PRINTHEAD_POST_KEY_AXES].append(ConstGeneral.PRINTHEAD_AXES_VALUE_Y)

    def enable_home_axes_z(self):
        self.enable_home_axes()
        if ConstGeneral.PRINTHEAD_AXES_VALUE_Z not in self._params[ConstGeneral.PRINTHEAD_POST_KEY_AXES]:
            self._params[ConstGeneral.PRINTHEAD_POST_KEY_AXES].append(ConstGeneral.PRINTHEAD_AXES_VALUE_Z)

    def set_command(self, command: str):
        self._params[ConstGeneral.POST_KEY_COMMAND] = command

    def set_speed(self, speed: int):
        self._params[ConstGeneral.PRINTHEAD_POST_KEY_SPEED] = speed

    def set_x(self, amount: int):
        self._params[ConstGeneral.PRINTHEAD_POST_KEY_X] = amount

    def set_y(self, amount: int):
        self._params[ConstGeneral.PRINTHEAD_POST_KEY_Y] = amount

    def set_z(self, amount: int):
        self._params[ConstGeneral.PRINTHEAD_POST_KEY_Z] = amount
