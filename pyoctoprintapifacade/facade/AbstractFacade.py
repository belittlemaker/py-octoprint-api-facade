from abc import ABCMeta
from ..config import Configuration
from ..request import FactoryRequest
from ..sender import Sender


class AbstractFacade(metaclass=ABCMeta):

    def __init__(self, config: Configuration, sender: Sender):
        self.__config: Configuration = config
        self.__factory_request = None
        self.__sender = sender

    def _get_factory_request(self) -> FactoryRequest:
        if self.__factory_request is None:
            self.__factory_request = FactoryRequest(self.__config)

        return self.__factory_request

    def _get_sender(self) -> Sender:
        return self.__sender
