from ..constant import ConstGeneral, ConstGCodes
from .AbstractFacade import AbstractFacade
from requests import Response


class PrinterFacade(AbstractFacade):
    """ https://docs.octoprint.org/en/master/api/printer.html """

    def get_printer_details(self) -> dict:
        response: Response = self._get_sender().send(self._get_factory_request().get_request_printer())
        return response.json()

    def printer_command(self, commands: list) -> str:
        request = self._get_factory_request().get_request_printer_command()
        request.set_params({ConstGeneral.POST_KEY_COMMANDS: commands})

        response: Response = self._get_sender().send(request)
        return response.text

    def go_home(self) -> None:
        self.printer_command([ConstGCodes.HOME])

    def power_off(self) -> None:
        self.printer_command([ConstGCodes.POWER_OFF])

    def power_on(self) -> None:
        self.printer_command([ConstGCodes.POWER_ON])

    def jog(self, x: int = 0, y: int = 0, z: int = 0, speed: int = None, absolute: bool = False):
        request = self._get_factory_request().get_request_printer_printhead()

        if absolute:
            request.enable_absolute()
        if type(speed) is int:
            request.set_speed(speed)
        request.set_x(x)
        request.set_y(y)
        request.set_z(z)
        request.set_command(ConstGeneral.PRINTHEAD_COMMAND_VALUE_JOG)

        self._get_sender().send(request)

    def move_axis_x(self, amount, speed: int = None, absolute: bool = False):
        self.jog(x=amount,
                 absolute=absolute,
                 speed=speed)

    def move_axis_y(self, amount, speed: int = None, absolute: bool = False):
        self.jog(y=amount,
                 absolute=absolute,
                 speed=speed)

    def move_axis_z(self, amount, speed: int = None, absolute: bool = False):
        self.jog(z=amount,
                 absolute=absolute,
                 speed=speed)

    def home_axis(self, x: bool = False, y: bool = False, z: bool = False):
        request = self._get_factory_request().get_request_printer_printhead()

        request.set_command(ConstGeneral.PRINTHEAD_COMMAND_VALUE_HOME)
        if not x and not y and not z:
            request.enable_home_axes_x()
            request.enable_home_axes_y()
            request.enable_home_axes_z()
        else:
            if x:
                request.enable_home_axes_x()
            if y:
                request.enable_home_axes_y()
            if z:
                request.enable_home_axes_z()

        self._get_sender().send(request)

    def bed_offset(self, offset: int):
        request = self._get_factory_request().get_request_printer_bed()
        request.set_params({ConstGeneral.POST_KEY_COMMAND: ConstGeneral.PRINTER_COMMAND_VALUE_OFFSET,
                            ConstGeneral.PRINTER_KEY_OFFSET: offset})

        self._get_sender().send(request)

    def bed_target(self, target: int):
        request = self._get_factory_request().get_request_printer_bed()
        request.set_params({ConstGeneral.POST_KEY_COMMAND: ConstGeneral.PRINTER_COMMAND_VALUE_TARGET,
                            ConstGeneral.PRINTER_KEY_TARGET: target})

        self._get_sender().send(request)

    def tool_offset(self, offsets: dict):
        request = self._get_factory_request().get_request_printer_tool()
        request.set_params({ConstGeneral.POST_KEY_COMMAND: ConstGeneral.PRINTER_COMMAND_VALUE_OFFSET,
                            ConstGeneral.PRINTER_KEY_OFFSETS: offsets})

        self._get_sender().send(request)

    def tool_target(self, targets: dict):
        request = self._get_factory_request().get_request_printer_tool()
        request.set_params({ConstGeneral.POST_KEY_COMMAND: ConstGeneral.PRINTER_COMMAND_VALUE_TARGET,
                            ConstGeneral.PRINTER_KEY_TARGETS: targets})

        self._get_sender().send(request)
