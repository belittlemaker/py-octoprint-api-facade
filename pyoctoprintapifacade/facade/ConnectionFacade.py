from ..constant import ConstGeneral
from .AbstractFacade import AbstractFacade
from requests import Response


class ConnectionFacade(AbstractFacade):
    """ https://docs.octoprint.org/en/master/api/connection.html """

    def get_connection_details(self) -> dict:
        response: Response = self._get_sender().send(self._get_factory_request().get_request_connection_get())
        return response.json()

    def connect(self, port: str = None, baudrate: int = None, printer_profile: str = None, save: bool = False, autoconnect: bool = False) -> None:
        params = {}
        if port:
            params[ConstGeneral.CONNECTION_POST_KEY_PORT] = port
        if baudrate:
            params[ConstGeneral.CONNECTION_POST_KEY_BAUDRATE] = baudrate
        if printer_profile:
            params[ConstGeneral.CONNECTION_POST_KEY_PRINTER_PROFILE] = printer_profile
        if save:
            params[ConstGeneral.CONNECTION_POST_KEY_SAVE] = save
        if autoconnect:
            params[ConstGeneral.CONNECTION_POST_KEY_AUTOCONNECT] = autoconnect

        self.__connection_command(params, ConstGeneral.CONNECTION_COMMAND_VALUE_CONNECT)

    def disconnect(self, params: dict = None) -> None:
        if params is None:
            params = {}
        self.__connection_command(params, ConstGeneral.CONNECTION_COMMAND_VALUE_DISCONNECT)

    def is_operational(self) -> bool:
        allowed_connection_states = [ConstGeneral.CONNECTION_STATE_CANCELLING,
                                     ConstGeneral.CONNECTION_STATE_ERROR,
                                     ConstGeneral.CONNECTION_STATE_OPERATIONAL,
                                     ConstGeneral.CONNECTION_STATE_PAUSING,
                                     ConstGeneral.CONNECTION_STATE_PAUSED,
                                     ConstGeneral.CONNECTION_STATE_PRINTING]
        connection_details = self.get_connection_details()

        return ('current' in connection_details
                and 'state' in connection_details['current']
                and str(connection_details['current']['state']) in allowed_connection_states)

    def __connection_command(self, params: dict, command: str) -> None:
        request = self._get_factory_request().get_request_connection_post()

        params[ConstGeneral.CONNECTION_POST_KEY_COMMAND] = command
        request.set_params(params)

        self._get_sender().send(request)
