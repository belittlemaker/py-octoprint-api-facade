from ..constant import ConstGeneral
from .AbstractFacade import AbstractFacade
from requests import Response


class JobFacade(AbstractFacade):
    """ https://docs.octoprint.org/en/master/api/job.html """

    def get_current_job(self) -> dict:
        response: Response = self._get_sender().send(self._get_factory_request().get_request_job())
        return response.json()

    def job_command(self, command: str, action: str = None):
        params = {ConstGeneral.POST_KEY_COMMAND: command}
        if action is not None:
            params[ConstGeneral.POST_KEY_ACTION] = action

        request = self._get_factory_request().get_request_job_post()
        request.set_params(params)

        self._get_sender().send(request)

    def cancel(self) -> None:
        self.job_command(ConstGeneral.JOB_COMMAND_VALUE_CANCEL)

    def pause(self) -> None:
        self.job_command(ConstGeneral.JOB_COMMAND_VALUE_PAUSE,
                         ConstGeneral.JOB_ACTION_VALUE_PAUSE)

    def pause_toggle(self) -> None:
        self.job_command(ConstGeneral.JOB_COMMAND_VALUE_PAUSE,
                         ConstGeneral.JOB_ACTION_VALUE_TOGGLE)

    def restart(self) -> None:
        self.job_command(ConstGeneral.JOB_COMMAND_VALUE_RESTART)

    def resume(self) -> None:
        self.job_command(ConstGeneral.JOB_COMMAND_VALUE_PAUSE,
                         ConstGeneral.JOB_ACTION_VALUE_RESUME)

    def start(self) -> None:
        self.job_command(ConstGeneral.JOB_COMMAND_VALUE_START)
