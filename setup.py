from setuptools import setup, find_packages

setup(name='pyoctoprintapifacade',
      packages=find_packages(),
      version='0.1.1+20240728',
      description='Facade for communication with printer through the Octoprint API',
      keywords=["octoprint", "printer"],
      author='BeLittleMaker',
      author_email='belittlemaker@gmail.com',
      url='https://gitlab.com/belittlemaker/py-octoprint-api-facade',
      license='GNU',
      install_requires=['requests'],
      classifiers=["Programming Language :: Python :: 3.8"])
